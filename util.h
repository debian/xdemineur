/*
 * Copyright � 1993-1999 Marc Baudoin <babafou@babafou.eu.org>
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby
 * granted, provided that the above copyright notice appear in all
 * copies and that both that copyright notice and this permission
 * notice appear in supporting documentation.  The author makes no
 * representations about the suitability of this software for any
 * purpose.  It is provided "as is" without express or implied
 * warranty.
 *
 */

/* $Id: util.h,v 1.1.1.1.2.1 1999/07/29 21:25:33 babafou Exp $ */

#ifndef _UTIL_H_
#define _UTIL_H_

#include <sys/types.h>

void *xmalloc ( size_t size ) ;

#endif /* _UTIL_H_ */
